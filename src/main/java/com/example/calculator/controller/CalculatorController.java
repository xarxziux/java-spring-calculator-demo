package com.example.calculator.controller;

import com.example.calculator.service.CalculatorService;
import com.example.calculator.utils.ResultStore;
import org.springframework.web.bind.annotation.*;

@RequestMapping("api/calculator")
@RestController
public class CalculatorController {
    @CrossOrigin
    @GetMapping(path = "{str}")
    public String getAnswer(@PathVariable("str") String input){
        ResultStore result = CalculatorService.getAnswerFromUserInput(input);

        return result.succeeded()
                ? result.getAnswer()
                : result.getException().getMessage();
    }
}
