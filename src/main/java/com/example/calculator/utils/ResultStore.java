package com.example.calculator.utils;

public class ResultStore {
    private String answer;
    private Exception exception;
    private boolean success;

    public ResultStore(String answer) {
        this.answer = answer;
        this.exception = null;
        this.success = true;
    }

    public ResultStore(Exception exception) {
        this.exception = exception;
        this.answer = null;
        this.success = false;
    }

    public String getAnswer() {
        return answer;
    }

    public Exception getException() {
        return exception;
    }

    public boolean succeeded() {
        return success;
    }
}
