package com.example.calculator.service;

import com.example.calculator.utils.ResultStore;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidParameterException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CalculatorService {
    static String decimalPattern = "([\\+\\-]?(?:\\d*)\\.?(?:\\d*))";
    static String whitespacePattern = "\\s*";
    static Pattern calcPattern = Pattern.compile("^"
            + whitespacePattern
            + decimalPattern
            + whitespacePattern
            + "([\\+\\-\\*_])"
            + whitespacePattern
            + decimalPattern
            + whitespacePattern
            + "$"
            );

    public static ResultStore getAnswerFromUserInput(String input){
        Matcher matcher = calcPattern.matcher(input);

        return matcher.find()
            ? calculateAnswer(matcher.group(1), matcher.group(3), matcher.group(2))
            : new ResultStore(new InvalidParameterException("Regex check failed for: " + input));
    }

    private static ResultStore calculateAnswer(String term1, String term2, String operator){
        try {
            BigDecimal dec1 = new BigDecimal(term1);
            BigDecimal dec2 = new BigDecimal(term2);

            return operator.equals("+")
                    ? new ResultStore(dec1.add(dec2).toPlainString())
                    : operator.equals("-")
                    ? new ResultStore(dec1.subtract(dec2).toPlainString())
                    : operator.equals("*")
                    ? new ResultStore(dec1.multiply(dec2).toPlainString())
                    : operator.equals("_")
                    ? new ResultStore(dec1.divide(dec2, 6, RoundingMode.DOWN)
                        .toPlainString())
                    : new ResultStore(
                            new InvalidParameterException("Unsupported operator: " + operator));
        } catch(Exception e){
            return new ResultStore(new Exception("Exception caught in calculateAnswer method", e));
        }
    }
}
