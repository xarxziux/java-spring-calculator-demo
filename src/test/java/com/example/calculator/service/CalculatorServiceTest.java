package com.example.calculator.service;

import com.example.calculator.utils.ResultStore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static com.example.calculator.service.CalculatorService.getAnswerFromUserInput;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class CalculatorServiceTest {
    String input;
    boolean expectedSuccess;
    String expectedResult;
    String expectedExceptionMessage;

    public CalculatorServiceTest(String input, boolean expectedSuccess, String expectedMessage) {
        this.input = input;
        this.expectedSuccess = expectedSuccess;

        if (expectedSuccess){
            this.expectedResult = expectedMessage;
            this.expectedExceptionMessage = null;
        } else {
            this.expectedResult = null;
            this.expectedExceptionMessage = expectedMessage;
        }
    }

    @Parameterized.Parameters
    public static Collection testData() {
        return Arrays.asList(new Object[][] {
                // input, SUCCESS/FAILURE, result

                // Basic integers
                {"123+456", SUCCESS, "579"},
                {"77385-2325", SUCCESS, "75060"},
                {"6823*78373", SUCCESS, "534738979"},
                {"994622_6745", SUCCESS, "147.460637"},

                // Bigger integers
                {"8836723685645346346867354+56245574567236645683453", SUCCESS, "8892969260212582992550807"},
                {"5745746256352880525375734-31234635745743534056655", SUCCESS, "5714511620607136991319079"},
                {"69342268375623246*6756324567993", SUCCESS, "468498871426587393383078365278"},
                {"623463457368327346846846_55456768856898357", SUCCESS, "11242332.905783"},

                // Signed integers
                {"+56373+-22357", SUCCESS, "34016"},
                {"-66812--96942", SUCCESS, "30130"},
                {"+88201*-37371", SUCCESS, "-3296159571"},
                {"-58599_-55515", SUCCESS, "1.055552"},

                // Decimals
                {"99425.5636+5621445.43566", SUCCESS, "5720870.99926"},
                {"-4521535446.34342-432452566.778585", SUCCESS, "-4953988013.122005"},
                {"58337.7678*34525.7998943", SUCCESS, "2014158097.34293794354"},
                {"56577884.453452_970663.1245", SUCCESS, "58.287868"},

                // Whitespace
                {"  8       +       4           ", SUCCESS, "12"},
                {"          8   \n  \t-       -4  \r\n", SUCCESS, "12"},
                {"\n\n\n 8  *  -4           ", SUCCESS, "-32"},
                // TODO: Strip trailing zeros from division results
                {"  8_4              ", SUCCESS, "2.000000"},

                // Malformed decimals
                {"8. + .4", SUCCESS, "8.4"},
                // FIXME: Actual answer is -12.00000000000
                //{"-8.00000000 - 4.00000000000", SUCCESS, "-12"},
                {"  -.008   *   -.0004     ", SUCCESS, "0.0000032"},
                // TODO: Strip trailing zeros from division results
                {"000000.8   _    -0000.004", SUCCESS, "-200.000000"},

                // Regex failures
                {"8+++4", FAILURE, REGEX_FAILURE},
                {"8/4", FAILURE, REGEX_FAILURE},
                {"8.1.2+1.2", FAILURE, REGEX_FAILURE},
                {"1+2+3", FAILURE, REGEX_FAILURE},
                {"Random String", FAILURE, REGEX_FAILURE}
        });
    }

    @Test
    public void testService() {
        ResultStore result = getAnswerFromUserInput(input);

        assertEquals(expectedSuccess, result.succeeded());

        if (expectedSuccess){
            assertEquals(expectedResult, result.getAnswer());
        } else {
            assertThat(result.getException().getMessage(), startsWith(expectedExceptionMessage));
        }
    }

    private static final boolean SUCCESS = true;
    private static final boolean FAILURE = false;
    private static final String REGEX_FAILURE = "Regex check failed for: ";
}
