# Spring Calculator Demo #

This is the backend service of a simple calculator app that performs basic operations on two numbers.  It is currently running here:
 
    https://spring-calculator-demo.herokuapp.com/api/calculator

To query it, simply add two integer or decimal numbers into the URL separated by one of "+", "-", "*" or "\_" and you should get the result of the operation back.  It currently uses the underscore ("_") in place of the divisor symbol as a workaround.

The app also allows for arbitrary whitespace characters to be included in the query, but not where they split either term.  Either term may also be signed with a "+" or "-", but with the caveat that there cannot be any whitespace between the sign and the term.

Internally, input terms are converted into "BigDecimal" types so in theory it can support very large numbers, but there may be some precision issues.

Here's a sample cURL session demonstrating some of the points given above, as well as the result of sending invalid input (note that the returned string does not include a line feed character):

    > curl spring-calculator-demo.herokuapp.com/api/calculator/2+3
    5.0> curl spring-calculator-demo.herokuapp.com/api/calculator/-1234+3456
    2222.0> curl spring-calculator-demo.herokuapp.com/api/calculator/-.5*-178
    89.00> curl spring-calculator-demo.herokuapp.com/api/calculator/4567%20_%20-.034
    -134323.529411> curl spring-calculator-demo.herokuapp.com/api/calculator/not_valid_input
    Regex check failed for: not_valid_input>
